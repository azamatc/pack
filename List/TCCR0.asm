
;CodeVisionAVR C Compiler V2.04.4a Advanced
;(C) Copyright 1998-2009 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega8
;Program type             : Application
;Clock frequency          : 1,000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 256 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : Yes
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega8
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1024
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _razr1=R4
	.DEF _razr2=R6
	.DEF _razr3=R8
	.DEF _razr4=R10
	.DEF _menu=R12

	.CSEG
	.ORG 0x00

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

;REGISTER BIT VARIABLES INITIALIZATION
__REG_BIT_VARS:
	.DW  0x0000

_0x3:
	.DB  0xC0,0x0,0xF9,0x0,0xA4,0x0,0xB0,0x0
	.DB  0x99,0x0,0x92,0x0,0x82,0x0,0xF8,0x0
	.DB  0x80,0x0,0x90,0x0,0xA3,0x0,0x8E,0x0
	.DB  0xAB,0x0,0xC6,0x0,0xC7,0x0,0x87,0x0
	.DB  0xE3,0x0,0xAF,0x0,0x89,0x0,0x83,0x0
	.DB  0xCF,0x0,0xFF,0x0,0x86,0x0,0x92,0x0
	.DB  0xA7,0x0,0xEF,0x0,0x88,0x0,0xC1
_0xE:
	.DB  0x57,0x4
_0x4F:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
	.DB  0x0,0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  0x02
	.DW  __REG_BIT_VARS*2

	.DW  0x37
	.DW  _digit
	.DW  _0x3*2

	.DW  0x02
	.DW  _app
	.DW  _0xE*2

	.DW  0x0A
	.DW  0x04
	.DW  _0x4F*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(0x400)
	LDI  R25,HIGH(0x400)
	LDI  R26,0x60
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;STACK POINTER INITIALIZATION
	LDI  R30,LOW(0x45F)
	OUT  SPL,R30
	LDI  R30,HIGH(0x45F)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(0x160)
	LDI  R29,HIGH(0x160)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x160

	.CSEG
;#include <mega8.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;#include <indicator.c>
;#define RAZRYAD PORTC    //======= �� ����� �����
; #define CHISLO PORTD    //======�� �����������
;
;unsigned int razr1=0,razr2=0,razr3=0,razr4=0;
;
;unsigned int digit [30]=
;
; {
;          (0b11000000),  //0
;          (0b11111001),  //1
;          (0b10100100),  //2
;          (0b10110000),  //3
;          (0b10011001),  //4
;          (0b10010010),  //5
;          (0b10000010),  //6
;          (0b11111000),  //7
;          (0b10000000),  //8
;          (0b10010000),  //9
;          (0b10100011),  //o 10
;          (0b10001110),  //F 11
;          (0b10101011),  //n 12
;          (0b11000110),  //C 13
;          (0b11000111),  //L 14
;          (0b10000111),  //t 15
;          (0b11100011),  //u 16
;          (0b10101111),  //r 17
;          (0b10001001),  //H 18
;          (0b10000011),  //b 19
;          (0b11001111),  //I 20
;          (0b11111111),   //empty 21
;          (0b10000110),  //E  22
;          (0b10010010),  //S  23
;          (0b10100111),  //c  24
;          (0b11101111),  //i  25
;          (0b10001000),  //A  26
;          (0b11000001),  //V  27
;
;
;//
;//        (0b00111111),   //0
;//        (0b00000110),   //1
;//        (0b01011011),   //2
;//        (0b01001111),   //3
;//        (0b01100110),   //4
;//        (0b01101101),   //5
;//        (0b01111101),   //6
;//        (0b00000111),   //7
;//        (0b01111111),   //8
;//        (0b01101111)    //9
;  };

	.DSEG
;
;  void show_numbers(unsigned int razbivka_chisla)
; 0000 0003   {

	.CSEG
_show_numbers:
;
;  razr1=razbivka_chisla/1000;
;	razbivka_chisla -> Y+0
	RCALL SUBOPT_0x0
	RCALL __DIVW21U
	MOVW R4,R30
;  razr2=razbivka_chisla%1000/100;
	RCALL SUBOPT_0x0
	RCALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL __DIVW21U
	MOVW R6,R30
;  razr3=razbivka_chisla%100/10;
	LD   R26,Y
	LDD  R27,Y+1
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL __MODW21U
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	RCALL __DIVW21U
	MOVW R8,R30
;  razr4=razbivka_chisla%10;
	LD   R26,Y
	LDD  R27,Y+1
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	RCALL __MODW21U
	MOVW R10,R30
;  }
	RJMP _0x2000001
;
;//interrupt [TIM1_OVF]          // ������� ������ �� ������������
;
;//void timer1_ovf_isr(void){
;// ������� �� ������������
;//   if (TCNT0==255){
;//   bc547=1;
;//           if (bc547==1){
;//              RAZRYAD=1;
;//              CHISLO= digit [razr1];
;//           }
;//           if (bc547==2){
;//              RAZRYAD=2;
;//              CHISLO= digit [razr2];
;//           }
;//           if (bc547==3){
;//              RAZRYAD=4;
;//              CHISLO= digit [razr3];
;//           }
;//           if (bc547==4){
;//              RAZRYAD=8;
;//              CHISLO= digit [razr4];
;//           }
;//           bc547++;
;//           if (bc547>4){
;//              bc547=1;
;//           };
;//   }
;//}
;  void razryadi(unsigned char bc547)
;{
_razryadi:
;       if (bc547==1){
;	bc547 -> Y+0
	LD   R26,Y
	CPI  R26,LOW(0x1)
	BRNE _0x4
;          RAZRYAD=1;
	LDI  R30,LOW(1)
	OUT  0x15,R30
;          CHISLO= digit [razr1];
	MOVW R30,R4
	RCALL SUBOPT_0x1
;       }
;       if (bc547==2){
_0x4:
	LD   R26,Y
	CPI  R26,LOW(0x2)
	BRNE _0x5
;          RAZRYAD=2;
	LDI  R30,LOW(2)
	OUT  0x15,R30
;          CHISLO= digit [razr2];
	MOVW R30,R6
	RCALL SUBOPT_0x1
;       }
;       if (bc547==3){
_0x5:
	LD   R26,Y
	CPI  R26,LOW(0x3)
	BRNE _0x6
;          RAZRYAD=4;
	LDI  R30,LOW(4)
	OUT  0x15,R30
;          CHISLO= digit [razr3];
	MOVW R30,R8
	RCALL SUBOPT_0x1
;       }
;       if (bc547==4){
_0x6:
	LD   R26,Y
	CPI  R26,LOW(0x4)
	BRNE _0x7
;          RAZRYAD=8;
	LDI  R30,LOW(8)
	OUT  0x15,R30
;          CHISLO= digit [razr4];
	MOVW R30,R10
	RCALL SUBOPT_0x1
;       }
;
;
;
;}
_0x7:
	ADIW R28,1
	RET
;//switch (vivod){
;//case 1:{
;//        RAZRYAD=1;
;//        CHISLO= digit [razr1];
;//       }
;//case 2:{
;//        RAZRYAD=2;
;//        CHISLO= digit [razr2];
;//       }
;//case 3:{
;//        RAZRYAD=4;
;//        CHISLO= digit [razr3];
;//       }
;//case 4:{
;//        RAZRYAD=8;
;//        CHISLO= digit [razr4];
;//       }
;
;//}
;void show_letters(
;   int index_of_digits_arr_element_one,
;   int index_of_digits_arr_element_two,
;   int index_of_digits_arr_element_three,
;   int index_of_digits_arr_element_four
;){
_show_letters:
;
;    razr1=index_of_digits_arr_element_one;
;	index_of_digits_arr_element_one -> Y+6
;	index_of_digits_arr_element_two -> Y+4
;	index_of_digits_arr_element_three -> Y+2
;	index_of_digits_arr_element_four -> Y+0
	__GETWRS 4,5,6
;    razr2=index_of_digits_arr_element_two;
	__GETWRS 6,7,4
;    razr3=index_of_digits_arr_element_three;
	__GETWRS 8,9,2
;    razr4=index_of_digits_arr_element_four;
	__GETWRS 10,11,0
;
;
;}
	ADIW R28,8
	RET
;
;//********************** eeprom *****************************************
;void EEPROM_write(unsigned int uiAddress, unsigned char ucData){
_EEPROM_write:
;/* Wait for completion of previous write */
;   while(EECR & (1<<EEWE));
;	uiAddress -> Y+1
;	ucData -> Y+0
_0x8:
	SBIC 0x1C,1
	RJMP _0x8
;/* Set up address and data registers */
;   EEAR = uiAddress;
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	OUT  0x1E+1,R31
	OUT  0x1E,R30
;   EEDR = ucData;
	LD   R30,Y
	OUT  0x1D,R30
;/* Write logical one to EEMWE */
;   EECR |= (1<<EEMWE);
	SBI  0x1C,2
;/* Start eeprom write by setting EEWE */
;   EECR |= (1<<EEWE);
	SBI  0x1C,1
;}
	ADIW R28,3
	RET
;
;
;unsigned char EEPROM_read(unsigned int uiAddress){
_EEPROM_read:
;/* Wait for completion of previous write */
;   while(EECR & (1<<EEWE));
;	uiAddress -> Y+0
_0xB:
	SBIC 0x1C,1
	RJMP _0xB
;/* Set up address register */
;   EEAR = uiAddress;
	LD   R30,Y
	LDD  R31,Y+1
	OUT  0x1E+1,R31
	OUT  0x1E,R30
;/* Start eeprom read by writing EERE */
;   EECR |= (1<<EERE);
	SBI  0x1C,0
;/* Return data from data register */
;   return EEDR;
	IN   R30,0x1D
_0x2000001:
	ADIW R28,2
	RET
;}
;//****************************************************************************
;
;#define plus PINC.4
;#define minus PINC.5
;#define o   10
;#define F   11
;#define n   12
;#define C	13
;#define L	14
;#define t 15
;#define u 16
;#define r 17
;#define H 18
;#define b 19
;#define I 20
;#define z 21//empty
;#define E 22
;#define S 23
;#define c 24
;#define i 25
;#define A 26
;#define V 27
;
;
;
;//#define podgotovka PORTD.1
;
;short menu = 0,app=1111,vremya=0,enter=0,obr_xod=0,bc547=0;

	.DSEG
; int sekund = 0, startSaryoYoqSekunda = 0;
;   //pplenka=0,
;   //    zplenka=0,
;   //    ozplenka=0,
;   //    rezplenka=0,
;   //    zavorach=0,
;   //    zadbokkraev=0,
;   //    zadnijkray=0,
;   //    zadverxkray=0;
;   bit podgotovka = 0;
;
;void main(void)
; 0000 002A {

	.CSEG
_main:
; 0000 002B   DDRB =0xff;
	LDI  R30,LOW(255)
	OUT  0x17,R30
; 0000 002C    PORTB=0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 002D 
; 0000 002E 
; 0000 002F 
; 0000 0030    // Clock value: 0,977 kHz
; 0000 0031    TCCR0 = 0x05;
	LDI  R30,LOW(5)
	OUT  0x33,R30
; 0000 0032    TCNT0 = 0x00;
	LDI  R30,LOW(0)
	OUT  0x32,R30
; 0000 0033  //==================indication\/====================================
; 0000 0034    DDRC |= ((1 << 0) | (1 << 1) | (1 << 2) | (1 << 3));
	IN   R30,0x14
	ORI  R30,LOW(0xF)
	OUT  0x14,R30
; 0000 0035    DDRC &= ~((1 << 4) | (1 << 5));
	IN   R30,0x14
	ANDI R30,LOW(0xCF)
	OUT  0x14,R30
; 0000 0036    RAZRYAD =0b0110000;//|= ((1 << 4) | (1 << 5)); //==========port c=====
	LDI  R30,LOW(48)
	OUT  0x15,R30
; 0000 0037    DDRD = 0xFF;
	LDI  R30,LOW(255)
	OUT  0x11,R30
; 0000 0038    RAZRYAD &= ~((1 << 0) | (1 << 1) | (1 << 2) | (1 << 3)); //==========port c=====
	IN   R30,0x15
	ANDI R30,LOW(0xF0)
	OUT  0x15,R30
; 0000 0039    CHISLO = 0x00;
	LDI  R30,LOW(0)
	OUT  0x12,R30
; 0000 003A //   TCCR0 |= (1 << 1);
; 0000 003B //   TCCR0 &= ~((1 << 0) | (1 << 2));
; 0000 003C //   TCNT1 = 0x00;
; 0000 003D //   TIMSK |= (1 << 0);
; 0000 003E //==================indication/\====================================
; 0000 003F //#asm("sei")
; 0000 0040 
; 0000 0041    while (1)
_0xF:
; 0000 0042    {
; 0000 0043       //======================�������� ����=======================================
; 0000 0044 
; 0000 0045 //      if (PINC .3 == 0 && PINC .4 == 0)
; 0000 0046 //      {
; 0000 0047 //         podgotovka = 1;
; 0000 0048 //      }
; 0000 0049 //      else
; 0000 004A //      {
; 0000 004B          podgotovka = 1;
	SET
	BLD  R2,0
; 0000 004C //      };
; 0000 004D 
; 0000 004E       /*=============================������� ���� ������=================================== */
; 0000 004F       while (podgotovka == 1)
_0x12:
	SBRS R2,0
	RJMP _0x14
; 0000 0050       {
; 0000 0051          if (TCNT0 == 255)
	IN   R30,0x32
	CPI  R30,LOW(0xFF)
	BRNE _0x15
; 0000 0052          {
; 0000 0053             TCNT0 = 0;
	LDI  R30,LOW(0)
	OUT  0x32,R30
; 0000 0054             sekund++;
	LDI  R26,LOW(_sekund)
	LDI  R27,HIGH(_sekund)
	RCALL SUBOPT_0x2
; 0000 0055              bc547++;
	LDI  R26,LOW(_bc547)
	LDI  R27,HIGH(_bc547)
	RCALL SUBOPT_0x2
; 0000 0056          }
; 0000 0057             if (bc547>4){
_0x15:
	LDS  R26,_bc547
	LDS  R27,_bc547+1
	SBIW R26,5
	BRLT _0x16
; 0000 0058           bc547=1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _bc547,R30
	STS  _bc547+1,R31
; 0000 0059        };
_0x16:
; 0000 005A           razryadi(bc547);
	LDS  R30,_bc547
	ST   -Y,R30
	RCALL _razryadi
; 0000 005B  //=============================================================================
; 0000 005C          if (sekund == 10)
	RCALL SUBOPT_0x3
	SBIW R26,10
	BRNE _0x17
; 0000 005D          {
; 0000 005E             PORTB .0 = 1;
	SBI  0x18,0
; 0000 005F             obr_xod = vremya;
	RCALL SUBOPT_0x4
	STS  _obr_xod,R30
	STS  _obr_xod+1,R31
; 0000 0060             if (TCNT0 == 255){
	IN   R30,0x32
	CPI  R30,LOW(0xFF)
	BRNE _0x1A
; 0000 0061             obr_xod--;
	LDI  R26,LOW(_obr_xod)
	LDI  R27,HIGH(_obr_xod)
	LD   R30,X+
	LD   R31,X+
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 0062             }
; 0000 0063          }
_0x1A:
; 0000 0064          if (obr_xod==0){
_0x17:
	LDS  R30,_obr_xod
	LDS  R31,_obr_xod+1
	SBIW R30,0
	BRNE _0x1B
; 0000 0065           PORTB .0 = 0;
	CBI  0x18,0
; 0000 0066          }
; 0000 0067   //=======================================================================================
; 0000 0068          if (sekund == 20)
_0x1B:
	RCALL SUBOPT_0x3
	SBIW R26,20
	BRNE _0x1E
; 0000 0069          {
; 0000 006A             PORTB .1 = 1;
	SBI  0x18,1
; 0000 006B          }
; 0000 006C          if (sekund == 30)
_0x1E:
	RCALL SUBOPT_0x3
	SBIW R26,30
	BRNE _0x21
; 0000 006D          {
; 0000 006E             PORTB .2 = 1;
	SBI  0x18,2
; 0000 006F 
; 0000 0070          }
; 0000 0071          if (sekund == 40)
_0x21:
	RCALL SUBOPT_0x3
	SBIW R26,40
	BRNE _0x24
; 0000 0072          {
; 0000 0073             PORTB .3 = 1;
	SBI  0x18,3
; 0000 0074 
; 0000 0075 
; 0000 0076          }
; 0000 0077          if (sekund == 50)
_0x24:
	RCALL SUBOPT_0x3
	SBIW R26,50
	BRNE _0x27
; 0000 0078          {
; 0000 0079 
; 0000 007A             PORTB .4 = 1;
	SBI  0x18,4
; 0000 007B          }
; 0000 007C          if (sekund == 60)
_0x27:
	RCALL SUBOPT_0x3
	SBIW R26,60
	BRNE _0x2A
; 0000 007D          {
; 0000 007E             PORTB .5 = 1;
	SBI  0x18,5
; 0000 007F 
; 0000 0080          }
; 0000 0081          if (sekund == 70)
_0x2A:
	RCALL SUBOPT_0x3
	CPI  R26,LOW(0x46)
	LDI  R30,HIGH(0x46)
	CPC  R27,R30
	BRNE _0x2D
; 0000 0082          {
; 0000 0083             PORTB .6 = 1;
	SBI  0x18,6
; 0000 0084 
; 0000 0085          }
; 0000 0086          if (sekund == 80)
_0x2D:
	RCALL SUBOPT_0x3
	CPI  R26,LOW(0x50)
	LDI  R30,HIGH(0x50)
	CPC  R27,R30
	BRNE _0x30
; 0000 0087          {
; 0000 0088             PORTB .7 = 1;
	SBI  0x18,7
; 0000 0089          }
; 0000 008A          if (sekund == 90)
_0x30:
	RCALL SUBOPT_0x3
	CPI  R26,LOW(0x5A)
	LDI  R30,HIGH(0x5A)
	CPC  R27,R30
	BRNE _0x33
; 0000 008B          {
; 0000 008C             PORTB =0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 008D             sekund=0;
	STS  _sekund,R30
	STS  _sekund+1,R30
; 0000 008E          }
; 0000 008F //         if (PINC .3 == 1)
; 0000 0090 //         {
; 0000 0091 //            podgotovka = 0;
; 0000 0092 //            PORTB = 0x00;
; 0000 0093 //         }
; 0000 0094 //         if (PINC .4 == 1)
; 0000 0095 //         {
; 0000 0096 //            if (TCNT0 == 255)
; 0000 0097 //            {
; 0000 0098 //               startSaryoYoqSekunda++;
; 0000 0099 //               TCNT0 = 0;
; 0000 009A //            }
; 0000 009B ////            else
; 0000 009C ////            {
; 0000 009D ////               while (startSaryoYoqSekunda == 170)
; 0000 009E ////               {
; 0000 009F ////                  podgotovka = 0;
; 0000 00A0 ////                  PORTB = 0x00;
; 0000 00A1 ////                  startSaryoYoqSekunda = 0;
; 0000 00A2 ////               }
; 0000 00A3 ////            }
; 0000 00A4 //         }
; 0000 00A5       }
_0x33:
	RJMP _0x12
_0x14:
; 0000 00A6 
; 0000 00A7       /*=============================������� ���� �����===========================*/
; 0000 00A8 
; 0000 00A9 
; 0000 00AA       //=================================���� �������� ������========================================
; 0000 00AB 
; 0000 00AC       switch (menu){
	MOVW R30,R12
; 0000 00AD       case 1 :{
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x37
; 0000 00AE 
; 0000 00AF          show_letters(o,F,b,r);
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	RCALL SUBOPT_0x5
	LDI  R30,LOW(11)
	LDI  R31,HIGH(11)
	RCALL SUBOPT_0x5
	LDI  R30,LOW(19)
	LDI  R31,HIGH(19)
	RCALL SUBOPT_0x5
	LDI  R30,LOW(17)
	LDI  R31,HIGH(17)
	RCALL SUBOPT_0x5
	RCALL _show_letters
; 0000 00B0          if(plus==0&&minus==0){
	RCALL SUBOPT_0x6
	BRNE _0x39
	RCALL SUBOPT_0x7
	BREQ _0x3A
_0x39:
	RJMP _0x38
_0x3A:
; 0000 00B1             enter=1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _enter,R30
	STS  _enter+1,R31
; 0000 00B2             delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00B3             vremya=EEPROM_read(0x04); // ��������� �������� �� ������
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	RCALL SUBOPT_0x5
	RCALL _EEPROM_read
	LDI  R31,0
	RCALL SUBOPT_0x9
; 0000 00B4          }
; 0000 00B5            while(enter==1){
_0x38:
_0x3B:
	LDS  R26,_enter
	LDS  R27,_enter+1
	SBIW R26,1
	BRNE _0x3D
; 0000 00B6 
; 0000 00B7 
; 0000 00B8                if (plus == 0){
	SBIC 0x13,4
	RJMP _0x3E
; 0000 00B9                   vremya++;
	RCALL SUBOPT_0xA
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00BA                   delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00BB                }
; 0000 00BC                if (minus == 0){
_0x3E:
	SBIC 0x13,5
	RJMP _0x3F
; 0000 00BD                   vremya--;
	RCALL SUBOPT_0xA
	SBIW R30,1
	ST   -X,R31
	ST   -X,R30
; 0000 00BE                   delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00BF                }
; 0000 00C0                if (vremya>=240){
_0x3F:
	LDS  R26,_vremya
	LDS  R27,_vremya+1
	CPI  R26,LOW(0xF0)
	LDI  R30,HIGH(0xF0)
	CPC  R27,R30
	BRLT _0x40
; 0000 00C1                   vremya=0;
	LDI  R30,LOW(0)
	STS  _vremya,R30
	STS  _vremya+1,R30
; 0000 00C2                }
; 0000 00C3                if (vremya<0){
_0x40:
	LDS  R26,_vremya+1
	TST  R26
	BRPL _0x41
; 0000 00C4                   vremya=240;
	LDI  R30,LOW(240)
	LDI  R31,HIGH(240)
	RCALL SUBOPT_0x9
; 0000 00C5                }
; 0000 00C6 
; 0000 00C7                show_numbers(vremya);
_0x41:
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x5
	RCALL _show_numbers
; 0000 00C8                if(plus==0&&minus==0){
	RCALL SUBOPT_0x6
	BRNE _0x43
	RCALL SUBOPT_0x7
	BREQ _0x44
_0x43:
	RJMP _0x42
_0x44:
; 0000 00C9                   delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00CA                   show_numbers(55);
	LDI  R30,LOW(55)
	LDI  R31,HIGH(55)
	RCALL SUBOPT_0x5
	RCALL _show_numbers
; 0000 00CB                   EEPROM_write(0x04,vremya);// ���������
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	RCALL SUBOPT_0x5
	LDS  R30,_vremya
	ST   -Y,R30
	RCALL _EEPROM_write
; 0000 00CC                   enter=0;
	LDI  R30,LOW(0)
	STS  _enter,R30
	STS  _enter+1,R30
; 0000 00CD                }
; 0000 00CE 
; 0000 00CF 
; 0000 00D0            }
_0x42:
	RJMP _0x3B
_0x3D:
; 0000 00D1 
; 0000 00D2 
; 0000 00D3       }
; 0000 00D4       break;
	RJMP _0x36
; 0000 00D5       //===========================2================================
; 0000 00D6       case 2:
_0x37:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BRNE _0x45
; 0000 00D7       {
; 0000 00D8          show_numbers(menu);
	ST   -Y,R13
	ST   -Y,R12
	RJMP _0x4E
; 0000 00D9       }
; 0000 00DA 
; 0000 00DB       break;
; 0000 00DC       //===================================3==============================
; 0000 00DD       case 3:
_0x45:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BRNE _0x46
; 0000 00DE       {
; 0000 00DF          show_numbers(menu);
	ST   -Y,R13
	ST   -Y,R12
	RJMP _0x4E
; 0000 00E0       }
; 0000 00E1       break;
; 0000 00E2       //===============================4===============================
; 0000 00E3       case 4:
_0x46:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x48
; 0000 00E4       {
; 0000 00E5          show_numbers(menu);
	ST   -Y,R13
	ST   -Y,R12
	RJMP _0x4E
; 0000 00E6       }
; 0000 00E7       break;
; 0000 00E8       //===================================5==============================
; 0000 00E9       default :show_numbers(app);
_0x48:
	LDS  R30,_app
	LDS  R31,_app+1
	RCALL SUBOPT_0x5
_0x4E:
	RCALL _show_numbers
; 0000 00EA       }
_0x36:
; 0000 00EB       if (plus == 0)
	SBIC 0x13,4
	RJMP _0x49
; 0000 00EC       {
; 0000 00ED 
; 0000 00EE          menu++;
	MOVW R30,R12
	ADIW R30,1
	MOVW R12,R30
; 0000 00EF          delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00F0       }
; 0000 00F1       if (minus == 0)
_0x49:
	SBIC 0x13,5
	RJMP _0x4A
; 0000 00F2       {
; 0000 00F3          menu--;
	MOVW R30,R12
	SBIW R30,1
	MOVW R12,R30
; 0000 00F4          delay_ms(200);
	RCALL SUBOPT_0x8
; 0000 00F5       }
; 0000 00F6       if (menu > 4)
_0x4A:
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	CP   R30,R12
	CPC  R31,R13
	BRGE _0x4B
; 0000 00F7       {
; 0000 00F8          menu = 0;
	CLR  R12
	CLR  R13
; 0000 00F9       }
; 0000 00FA       if (menu < 0)
_0x4B:
	CLR  R0
	CP   R12,R0
	CPC  R13,R0
	BRGE _0x4C
; 0000 00FB       {
; 0000 00FC          menu = 4;
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	MOVW R12,R30
; 0000 00FD       }
; 0000 00FE 
; 0000 00FF       //===========================���� �������� �����=================================
; 0000 0100 
; 0000 0101    }; //while
_0x4C:
	RJMP _0xF
; 0000 0102 } // void main
_0x4D:
	RJMP _0x4D

	.DSEG
_digit:
	.BYTE 0x3C
_app:
	.BYTE 0x2
_vremya:
	.BYTE 0x2
_enter:
	.BYTE 0x2
_obr_xod:
	.BYTE 0x2
_bc547:
	.BYTE 0x2
_sekund:
	.BYTE 0x2

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x0:
	LD   R26,Y
	LDD  R27,Y+1
	LDI  R30,LOW(1000)
	LDI  R31,HIGH(1000)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x1:
	LDI  R26,LOW(_digit)
	LDI  R27,HIGH(_digit)
	LSL  R30
	ROL  R31
	ADD  R26,R30
	ADC  R27,R31
	LD   R30,X
	OUT  0x12,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2:
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:22 WORDS
SUBOPT_0x3:
	LDS  R26,_sekund
	LDS  R27,_sekund+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4:
	LDS  R30,_vremya
	LDS  R31,_vremya+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 15 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x5:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6:
	LDI  R26,0
	SBIC 0x13,4
	LDI  R26,1
	CPI  R26,LOW(0x0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x7:
	LDI  R26,0
	SBIC 0x13,5
	LDI  R26,1
	CPI  R26,LOW(0x0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x8:
	LDI  R30,LOW(200)
	LDI  R31,HIGH(200)
	RCALL SUBOPT_0x5
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x9:
	STS  _vremya,R30
	STS  _vremya+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xA:
	LDI  R26,LOW(_vremya)
	LDI  R27,HIGH(_vremya)
	LD   R30,X+
	LD   R31,X+
	RET


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xFA
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__MODW21U:
	RCALL __DIVW21U
	MOVW R30,R26
	RET

;END OF CODE MARKER
__END_OF_CODE:
